package com.ohobeds.repository;

import com.ohobeds.entity.RoomTypeFacility;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface RoomTypeFacilityRepository extends JpaRepository<RoomTypeFacility, Integer> {

  Optional<RoomTypeFacility> findFirstByFacilityId(long facilityId);
}
