package com.ohobeds.repository;

import com.ohobeds.entity.Customer;
import com.ohobeds.entity.Property;
import com.ohobeds.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

  List<Reservation> findAllByCustomer(Customer customer);

  List<Reservation> findAllByProperty(Property property);
}
