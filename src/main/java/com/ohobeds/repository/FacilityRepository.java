package com.ohobeds.repository;

import com.ohobeds.entity.Facility;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FacilityRepository extends JpaRepository<Facility, Integer> {

  List<Facility> findAll();

  Optional<Facility> getFirstById(Long facilityId);

  Facility save(Facility facility);
}
