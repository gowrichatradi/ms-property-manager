package com.ohobeds.repository;

import com.ohobeds.entity.RoomType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RoomTypeRepository extends JpaRepository<RoomType, Integer> {
  // select * from RoomType where propertyId = ""
  List<RoomType> findAllByPropertyId(long propertyId);

  Optional<RoomType> findFirstById(long roomTypeId);

  // select * from RoomType
  List<RoomType> findAll();

  // select * from RoomType where propertyId = "" and id = ""
  Optional<RoomType> findByPropertyIdAndId(long propertyId, long id);

  // insert into RoomType (column1, columns2, etc) values (a, b, etc)
  RoomType save(RoomType roomType);

  // Delete from RoomTyoe where propertyId = "" and id = ""
  void delete(RoomType roomType);


  Optional<RoomType> findFirstByPropertyIdAndId(long propertyId, long roomTypeId);
}
