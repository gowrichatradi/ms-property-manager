package com.ohobeds.repository;

import com.ohobeds.entity.Room;
import com.ohobeds.entity.RoomType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RoomRepository extends JpaRepository<Room, Integer> {
  List<Room> findAllByPropertyId(long propertyId);

  List<Room> findByRoomType(RoomType roomTypeId);

  Optional<Room> findFirstById(long roomTypeId);

  List<Room> findAllByRoomTypeId(long roomTypeId);

  List<Room> findAllByPropertyIdAndRoomTypeId(long propertyId, long roomTypeId);
}
