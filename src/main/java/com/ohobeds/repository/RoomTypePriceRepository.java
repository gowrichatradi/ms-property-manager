package com.ohobeds.repository;

import com.ohobeds.entity.RoomTypePrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomTypePriceRepository extends JpaRepository<RoomTypePrice, Integer> {

  List<RoomTypePrice> findAllByRoomTypeId(long roomTypeId);

  Optional<RoomTypePrice> findFirstById(long roomTypePriceId);

}
