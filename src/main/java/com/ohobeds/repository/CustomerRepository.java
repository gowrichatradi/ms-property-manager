package com.ohobeds.repository;

import com.ohobeds.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
  Optional<Customer> findFirstById(long id);

  Optional<Customer> findFirstByPhoneNumber(String phoneNumber);
}
