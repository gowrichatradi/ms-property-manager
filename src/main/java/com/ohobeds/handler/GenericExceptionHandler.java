//package com.ohobeds.handler;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.ResponseStatus;
//
//@ControllerAdvice
//public class GenericExceptionHandler {
//  @ExceptionHandler(Exception.class)
//  @ResponseStatus(value = HttpStatus.NOT_FOUND)
//  public @ResponseBody String handleAllErrors(final Exception exception) {
//    return exception.getMessage();
//  }
//}
