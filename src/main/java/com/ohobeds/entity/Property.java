package com.ohobeds.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@Entity
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Property {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @JsonIgnore
  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "property", orphanRemoval = true)
  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
  private Set<Reservation> reservations;

  @JsonIgnore
  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "property", orphanRemoval = true)
  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
  private Set<Customer> customers;

  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "property", orphanRemoval = true)
  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
  private Set<RoomType> roomTypes;

  @LazyCollection(LazyCollectionOption.FALSE)
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "property", orphanRemoval = true)
  private Set<Room> rooms;

  private String name;
  private String addressLine1;
  private String addressLine2;
  private String addressLine3;
  private String city;
  private String state;
  private String country;
  private int postCode;
  private float longitude;
  private float latitude;
  private String properType;
  private LocalDate startDate;
  private LocalDate endDate;
  private int clusterId;
  private String phoneNumber1;
  private String phoneNumber2;
  private String email;
}
