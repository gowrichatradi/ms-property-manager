package com.ohobeds.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@Entity
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Customer {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "property_id")
  @JsonIgnore
  private Property property;

  private String firstName;
  private String lastName;
  private String phoneNumber;
  private String email;

  private LocalDate dateOfBirth;
  private String gender;
  private String addressLine1;
  private String addressLine2;
  private String addressLine3;
  private String city;
  private String state;
  private String country;
  private int postCode;
  private String passport;
  private String driversLicense;
  private String identityDocument1;
  private String identityDocument2;
  private String identityDocument3;
  private String purposeOfVisit;
}
