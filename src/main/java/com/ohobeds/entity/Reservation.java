package com.ohobeds.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@NoArgsConstructor
@Entity
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Reservation {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  private String code;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "property_id")
  private Property property;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "room_id")
  private Room room;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "room_type_id")
  private RoomType roomType;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "customer_id")
  private Customer customer;

  private LocalDateTime createdOn;
  private LocalDate startDate;
  private LocalDate endDate;
  private int statusId;
  private int noOfGuests;
  private Float reservationPrice;
}
