package com.ohobeds.controller;

import com.ohobeds.entity.RoomTypePrice;
import com.ohobeds.service.RoomTypePriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

// TODO : Need to change the domain once deployed in cluster.
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/v1/pms/properties")
@RequiredArgsConstructor
public class RoomTypePriceController {

  private final RoomTypePriceService roomTypePriceService;

  @GetMapping(value = "/room-type-price")
  public ResponseEntity<?> getAllRoomTypePrices() {
    return ResponseEntity.ok(roomTypePriceService.getAllRoomTypePrices());
  }

  @GetMapping(value = "/{roomTypeId}/room-type-price")
  public ResponseEntity<?> getRoomTypePricesByRoomTypeId(
      @PathVariable("roomTypeId") long roomTypeId) {
    return ResponseEntity.ok(roomTypePriceService.getPricesByRoomType(roomTypeId));
  }

  @GetMapping(value = "/room-type-price/{roomTypePriceId}")
  public ResponseEntity<?> getRoomTypePriceById(
      @PathVariable("roomTypePriceId") long roomTypePriceId) {
    return ResponseEntity.ok(roomTypePriceService.getRoomTypePriceById(roomTypePriceId));
  }

  @PostMapping(
      value = "/{roomTypeId}/room-type-price",
      consumes = APPLICATION_JSON_VALUE,
      produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createRoomTypePrice(
      @Valid @RequestBody RoomTypePrice roomTypePrice,
      @PathVariable("roomTypeId") long roomTypeId) {

    return ResponseEntity.ok(roomTypePriceService.createRoomTypePrice(roomTypeId, roomTypePrice));
  }
}
