package com.ohobeds.controller;

import com.ohobeds.entity.RoomType;
import com.ohobeds.service.RoomTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

// TODO : Need to change the domain once deployed in cluster.
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/v1/pms/properties")
@RequiredArgsConstructor
public class RoomTypeController {

  private final RoomTypeService roomTypeService;

  @GetMapping(value = "/{propertyId}/room-types")
  public ResponseEntity<?> getAllRoomTypesByPropertyId(
      @PathVariable("propertyId") long propertyId) {
    return ResponseEntity.ok(roomTypeService.getAllRoomTypesByPropertyId(propertyId));
  }

  @GetMapping(value = "/{propertyId}/room-types/{roomTypeId}")
  public ResponseEntity<?> getRoomTypeById(
      @PathVariable("propertyId") int propertyId, @PathVariable("roomTypeId") int roomTypeId) {
    return ResponseEntity.ok(
        roomTypeService.getRoomTypeByPropertyIdAndRoomTypeId(propertyId, roomTypeId));
  }

  @PostMapping(
      value = "/{propertyId}/room-types",
      consumes = APPLICATION_JSON_VALUE,
      produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createRoomType(
      @Valid @RequestBody RoomType roomType, @PathVariable("propertyId") int propertyId) {
    return ResponseEntity.ok(roomTypeService.createRoomType(propertyId, roomType));
  }
}
