package com.ohobeds.controller;

import com.ohobeds.entity.RoomTypeFacility;
import com.ohobeds.service.RoomTypeFacilityService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

// TODO : Need to change the domain once deployed in cluster.
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/v1/pms/properties")
@RequiredArgsConstructor
public class RoomTypeFacilityController {

  final RoomTypeFacilityService roomTypeFacilityService;

  @GetMapping(value = "/room-types/{facilityId}")
  public ResponseEntity<?> getRoomTypeById(@PathVariable("facilityId") long facilityId) {
    return ResponseEntity.ok(roomTypeFacilityService.getRoomTypeFacilityById(facilityId));
  }

  @PostMapping(
      value = "/{roomTypeId}/room-type-facilities",
      consumes = APPLICATION_JSON_VALUE,
      produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createRoomTypeFacility(
      @Valid @RequestBody RoomTypeFacility roomTypeFacility,
      @PathVariable("roomTypeId") long roomTypeId) {

    return ResponseEntity.ok(
        roomTypeFacilityService.createRoomTypeFacility(roomTypeId, roomTypeFacility));
  }
}
