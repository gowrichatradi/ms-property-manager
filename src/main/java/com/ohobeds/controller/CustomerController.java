package com.ohobeds.controller;

import com.ohobeds.entity.Customer;
import com.ohobeds.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/v1/pms/properties")
@RequiredArgsConstructor
public class CustomerController {

  final CustomerService customerService;

  @GetMapping(value = "/customers/{customerId}")
  public ResponseEntity<?> getCustomer(@PathVariable("customerId") long customerId) {
    return ResponseEntity.ok(customerService.getCustomer(customerId));
  }

  @GetMapping(value = "/customers")
  public ResponseEntity<?> getCustomerByPhoneNumber(
      @RequestParam("phone") String phoneNumber) {
    return ResponseEntity.ok(customerService.getCustomerByPhoneNumber(phoneNumber));
  }

  @PostMapping(
      value = "/customers",
      consumes = APPLICATION_JSON_VALUE,
      produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createCustomer(@Valid @RequestBody Customer customer) {
    return ResponseEntity.ok(customerService.createCustomer(customer));
  }


  @PutMapping(
          value = "/customers",
          consumes = APPLICATION_JSON_VALUE,
          produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<?> updateCustomer(@Valid @RequestBody Customer customer) {
    return ResponseEntity.ok(customerService.updateCustomer(customer));
  }
}
