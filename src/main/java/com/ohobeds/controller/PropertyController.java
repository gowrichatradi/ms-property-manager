package com.ohobeds.controller;

import com.ohobeds.entity.Property;
import com.ohobeds.service.PropertyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

// TODO : Need to change the domain once deployed in cluster.
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/v1/pms/")
@RequiredArgsConstructor
public class PropertyController {

  private final PropertyService propertyService;

  @PostMapping(
      value = "/properties",
      consumes = APPLICATION_JSON_VALUE,
      produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createProperty(@Valid @RequestBody Property property) {
    return ResponseEntity.ok(propertyService.createProperty(property));
  }

  @GetMapping(value = "/properties")
  public ResponseEntity<?> getAllProperties() {
    return ResponseEntity.ok(propertyService.getProperties());
  }

  @GetMapping(value = "/properties/{propertyId}")
  public ResponseEntity<?> getPropertyById(@PathVariable("propertyId") long propertyId) {
    return ResponseEntity.ok(propertyService.getPropertyById(propertyId));
  }
}
