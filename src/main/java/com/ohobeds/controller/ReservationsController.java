package com.ohobeds.controller;

import com.ohobeds.entity.Reservation;
import com.ohobeds.service.ReservationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/v1/pms/properties")
@RequiredArgsConstructor
public class ReservationsController {

  final ReservationService reservationService;

  @GetMapping(value = "{propertyId}/reservations")
  public ResponseEntity<?> getReservation(@PathVariable("propertyId") long propertyId) {
    return ResponseEntity.ok().body(reservationService.findAllByPropertyId(propertyId));
  }

  @PostMapping(
      consumes = APPLICATION_JSON_VALUE,
      produces = APPLICATION_JSON_VALUE,
      value = "{propertyId}/{roomTypeId}/{roomId}/reservations")
  public ResponseEntity<?> addReservation(
      @Valid @RequestBody Reservation reservation,
      @PathVariable("propertyId") long propertyId,
      @PathVariable("roomTypeId") long roomTypeId,
      @PathVariable("roomId") long roomId) {
    return ResponseEntity.ok()
        .body(reservationService.addReservation(propertyId, roomTypeId, roomId, reservation));
  }
}
