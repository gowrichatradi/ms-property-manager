package com.ohobeds.controller;

import com.ohobeds.entity.Facility;
import com.ohobeds.service.FacilityService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

// TODO : Need to change the domain once deployed in cluster.
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/v1/pms/properties")
@RequiredArgsConstructor
public class FacilityController {

  private final FacilityService facilityService;

  @GetMapping(value = "/facilities")
  public ResponseEntity<?> getAllFacilities() {
    return ResponseEntity.ok(facilityService.getAllFacilities());
  }

  @GetMapping(value = "/facilities/{facilityId}")
  public ResponseEntity<?> getFacilityById(@PathVariable("facilityId") long facilityId) {
    return ResponseEntity.ok(facilityService.getFacilityById(facilityId));
  }

  @PostMapping(
      value = "/facilities",
      consumes = APPLICATION_JSON_VALUE,
      produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createFacility(@Valid @RequestBody Facility facility) {
    return ResponseEntity.ok(facilityService.createFacility(facility));
  }
}
