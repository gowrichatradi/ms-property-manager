package com.ohobeds.controller;

import com.ohobeds.entity.Room;
import com.ohobeds.service.RoomService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

// TODO : Need to change the domain once deployed in cluster.
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/v1/pms/properties")
@RequiredArgsConstructor
@Slf4j
public class RoomController {

  private final RoomService roomService;

  @GetMapping(value = "/{propertyId}/rooms")
  public ResponseEntity<?> getAllRoomsByPropertyId(@PathVariable("propertyId") long propertyId) {
    return ResponseEntity.ok(roomService.getAllRoomsByPropertyId(propertyId));
  }

  @GetMapping(value = "/{propertyId}/room-types/{roomTypeId}/rooms")
  public ResponseEntity<?> getRoomsByRoomType(
      @PathVariable("propertyId") long propertyId, @PathVariable("roomTypeId") long roomTypeId) {

    return ResponseEntity.ok(
        roomService.getAllRoomsByPropertyIdAndRoomTypeId(propertyId, roomTypeId));
  }

  @GetMapping(value = "/rooms")
  public ResponseEntity<?> getAllRooms() {
    return ResponseEntity.ok(roomService.getAllRooms());
  }

  @GetMapping(value = "/rooms/{roomId}")
  public ResponseEntity<?> getRoomById(@PathVariable("roomId") long roomId) {
    return ResponseEntity.ok(roomService.getRoomById(roomId));
  }

  @PostMapping(
      value = "/{propertyId}/room-types/{roomTypeId}/rooms",
      consumes = APPLICATION_JSON_VALUE,
      produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createRoom(
      @Valid @RequestBody Room room,
      @PathVariable("propertyId") long propertyId,
      @PathVariable("roomTypeId") long roomTypeId) {
    return ResponseEntity.ok(roomService.createRoom(propertyId, roomTypeId, room));
  }
}
