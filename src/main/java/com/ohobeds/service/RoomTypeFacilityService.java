package com.ohobeds.service;

import com.ohobeds.entity.RoomTypeFacility;

public interface RoomTypeFacilityService {
  RoomTypeFacility getRoomTypeFacilityById(long facilityId);

  RoomTypeFacility createRoomTypeFacility(long roomTypeId, RoomTypeFacility roomTypeFacility);
}
