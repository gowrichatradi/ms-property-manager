package com.ohobeds.service.impl;

import com.ohobeds.entity.RoomType;
import com.ohobeds.entity.RoomTypePrice;
import com.ohobeds.repository.RoomTypePriceRepository;
import com.ohobeds.service.RoomTypePriceService;
import com.ohobeds.service.RoomTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Component
@RequiredArgsConstructor
public class RoomTypePriceServiceImpl implements RoomTypePriceService {
  final RoomTypePriceRepository roomTypePriceRepository;
  final RoomTypeService roomTypeService;

  @Override
  public List<RoomTypePrice> getAllRoomTypePrices() {
    List<RoomTypePrice> roomTypePriceList = roomTypePriceRepository.findAll();

    if (CollectionUtils.isEmpty(roomTypePriceList))
      throw new RuntimeException("no room prices found");

    return roomTypePriceList;
  }

  @Override
  public List<RoomTypePrice> getPricesByRoomType(long roomTypeId) {
    List<RoomTypePrice> roomTypePriceList = roomTypePriceRepository.findAllByRoomTypeId(roomTypeId);
    if (CollectionUtils.isEmpty(roomTypePriceList))
      throw new RuntimeException("no room prices with roomtype id " + roomTypeId + " not found");

    return roomTypePriceList;
  }

  @Override
  public RoomTypePrice getRoomTypePriceById(long roomTypePriceId) {
    return roomTypePriceRepository
        .findFirstById(roomTypePriceId)
        .orElseThrow(
            () -> new RuntimeException("No room type price found for the id " + roomTypePriceId));
  }

  @Override
  public RoomTypePrice createRoomTypePrice(long roomTypeId, RoomTypePrice roomTypePrice) {
    RoomType roomType = roomTypeService.getRoomTypeById(roomTypeId);
    roomTypePrice.setRoomType(roomType);
    return roomTypePriceRepository.save(roomTypePrice);
  }
}
