package com.ohobeds.service.impl;

import com.ohobeds.entity.Property;
import com.ohobeds.entity.Room;
import com.ohobeds.entity.RoomType;
import com.ohobeds.repository.RoomRepository;
import com.ohobeds.service.PropertyService;
import com.ohobeds.service.RoomService;
import com.ohobeds.service.RoomTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@RequiredArgsConstructor
@Component
public class RoomServiceImpl implements RoomService {

  final RoomRepository roomRepository;

  final PropertyService propertyService;
  final RoomTypeService roomTypeService;

  @Override
  public List<Room> getAllRooms() {
    List<Room> roomList = roomRepository.findAll();
    if (CollectionUtils.isEmpty(roomList)) throw new RuntimeException("no rooms found");
    return roomList;
  }

  @Override
  public Room getRoomById(long roomId) {
    return roomRepository
        .findFirstById(roomId)
        .orElseThrow(() -> new RuntimeException("room with id " + roomId + " is not found"));
  }

  @Override
  public List<Room> getAllRoomsByPropertyId(long propertyId) {

    List<Room> roomList = roomRepository.findAllByPropertyId(propertyId);

    if (CollectionUtils.isEmpty(roomList))
      throw new RuntimeException("no rooms found for the property id " + propertyId);

    return roomList;
  }

  @Override
  public List<Room> getAllRoomsByRoomTypeId(long roomTypeId) {
    List<Room> roomList = roomRepository.findAllByRoomTypeId(roomTypeId);

    if (CollectionUtils.isEmpty(roomList))
      throw new RuntimeException("no rooms found for the roomtype id " + roomTypeId);

    return roomList;
  }

  @Override
  public List<Room> getAllRoomsByPropertyIdAndRoomTypeId(long propertyId, long roomTypeId) {
    List<Room> roomList = roomRepository.findAllByPropertyIdAndRoomTypeId(propertyId, roomTypeId);

    if (CollectionUtils.isEmpty(roomList))
      throw new RuntimeException(
          "no rooms found for the combination of the property id + "
              + propertyId
              + " and roomType id "
              + roomTypeId);

    return roomList;
  }

  @Override
  public Room createRoom(long propertyId, long roomTypeId, Room room) {
    Property property = propertyService.getPropertyById(propertyId);
    RoomType roomtype = roomTypeService.getRoomTypeById(roomTypeId);
    room.setProperty(property);
    room.setRoomType(roomtype);

    return roomRepository.save(room);
  }
}
