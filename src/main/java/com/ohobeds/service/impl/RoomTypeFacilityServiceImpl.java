package com.ohobeds.service.impl;

import com.ohobeds.entity.RoomType;
import com.ohobeds.entity.RoomTypeFacility;
import com.ohobeds.repository.RoomTypeFacilityRepository;
import com.ohobeds.service.RoomTypeFacilityService;
import com.ohobeds.service.RoomTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RoomTypeFacilityServiceImpl implements RoomTypeFacilityService {
  final RoomTypeFacilityRepository roomTypeFacilityRepository;
  final RoomTypeService roomTypeService;

  @Override
  public RoomTypeFacility getRoomTypeFacilityById(long facilityId) {
    return roomTypeFacilityRepository
        .findFirstByFacilityId(facilityId)
        .orElseThrow(
            () ->
                new RuntimeException("no room type facility found with facility id " + facilityId));
  }

  @Override
  public RoomTypeFacility createRoomTypeFacility(
      long roomTypeId, RoomTypeFacility roomTypeFacility) {

    RoomType roomType = roomTypeService.getRoomTypeById(roomTypeId);
    roomTypeFacility.setRoomType(roomType);

    return roomTypeFacilityRepository.save(roomTypeFacility);
  }
}
