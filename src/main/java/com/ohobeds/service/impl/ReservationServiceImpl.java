package com.ohobeds.service.impl;

import com.ohobeds.entity.*;
import com.ohobeds.repository.ReservationRepository;
import com.ohobeds.service.PropertyService;
import com.ohobeds.service.ReservationService;
import com.ohobeds.service.RoomService;
import com.ohobeds.service.RoomTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@Component
public class ReservationServiceImpl implements ReservationService {

  final ReservationRepository reservationRepository;
  final RoomTypeService roomTypeService;
  final RoomService roomService;
  final PropertyService propertyService;

  @Override
  public List<Reservation> findAll() {
    List<Reservation> allReservations = reservationRepository.findAll();
    if (allReservations.isEmpty()) throw new RuntimeException("no reservations found");
    return allReservations;
  }

  @Override
  public List<Reservation> findAllByPropertyId(long propertyId) {

    Property propertyById = propertyService.getPropertyById(propertyId);

    List<Reservation> allReservations = reservationRepository.findAllByProperty(propertyById);

    if (allReservations.isEmpty())
      throw new RuntimeException("no reservations found for the property id " + propertyId);
    return allReservations;
  }

  @Override
  public List<Reservation> findAllByCustomer(Customer customer) {
    List<Reservation> allReservations = reservationRepository.findAllByCustomer(customer);
    if (allReservations.isEmpty())
      throw new RuntimeException("no reservations found for the customer id " + customer.getId());
    return allReservations;
  }

  @Override
  public Reservation addReservation(
      long propertyId, long roomTypeId, long roomId, @Valid Reservation reservation) {

    Property property = propertyService.getPropertyById(propertyId);
    Room room = roomService.getRoomById(roomId);
    RoomType roomType = roomTypeService.getRoomTypeById(roomTypeId);

    reservation.setProperty(property);
    reservation.setRoom(room);
    reservation.setRoomType(roomType);

    reservationRepository.save(reservation);

    return null;
  }
}
