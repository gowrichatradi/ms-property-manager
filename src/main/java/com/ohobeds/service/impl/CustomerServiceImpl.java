package com.ohobeds.service.impl;

import com.ohobeds.entity.Customer;
import com.ohobeds.repository.CustomerRepository;
import com.ohobeds.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@RequiredArgsConstructor
@Component
public class CustomerServiceImpl implements CustomerService {

  private final CustomerRepository customerRepository;

  @Override
  public List<Customer> getAllCustomers() {
    List<Customer> allCustomers = customerRepository.findAll();

    if (CollectionUtils.isEmpty(allCustomers)) {
      throw new RuntimeException("no customers found");
    }

    return allCustomers;
  }

  @Override
  public Customer getCustomer(long customerId) {
    return customerRepository
        .findFirstById(customerId)
        .orElseThrow(
            () -> new RuntimeException("Cannot find the customer with customerId : " + customerId));
  }

  @Override
  public Customer getCustomerByPhoneNumber(String phoneNumber) {
    return customerRepository
        .findFirstByPhoneNumber(phoneNumber)
        .orElseThrow(
            () ->
                new RuntimeException(
                    "customer with the phone number " + phoneNumber + " not found"));
  }

  @Override
  public Customer createCustomer(Customer customer) {
    return customerRepository.save(customer);
  }

  @Override
  public Customer updateCustomer(Customer customer) {

    return customerRepository
        .findFirstById(customer.getId())
        .map(
            customer1 -> {
              customerRepository.delete(customer1);
              return customerRepository.save(customer1);
            })
        .orElse(customerRepository.save(customer));
  }
}
