package com.ohobeds.service.impl;

import com.ohobeds.entity.Facility;
import com.ohobeds.repository.FacilityRepository;
import com.ohobeds.service.FacilityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Component
@RequiredArgsConstructor
public class FacilityServiceImpl implements FacilityService {

  private final FacilityRepository facilityRepository;

  @Override
  public List<Facility> getAllFacilities() {
    List<Facility> facilityList = facilityRepository.findAll();
    if (CollectionUtils.isEmpty(facilityList)) throw new RuntimeException("no facilities found");

    return facilityList;
  }

  @Override
  public Facility getFacilityById(long facilityId) {

    return facilityRepository
        .getFirstById(facilityId)
        .orElseThrow(() -> new RuntimeException("no facility found with id " + facilityId));
  }

  @Override
  public Facility createFacility(Facility facility) {
    return facilityRepository.save(facility);
  }
}
