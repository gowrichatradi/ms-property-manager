package com.ohobeds.service.impl;

import com.ohobeds.entity.Property;
import com.ohobeds.entity.RoomType;
import com.ohobeds.repository.RoomTypeRepository;
import com.ohobeds.service.PropertyService;
import com.ohobeds.service.RoomTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Component
@RequiredArgsConstructor
public class RoomTypeServiceImpl implements RoomTypeService {

  final RoomTypeRepository roomTypeRepository;
  final PropertyService propertyService;

  @Override
  public RoomType getRoomTypeById(long roomTypeId) {
    return roomTypeRepository
        .findFirstById(roomTypeId)
        .orElseThrow(() -> new RuntimeException("Cannot find room type with id + " + roomTypeId));
  }

  @Override
  public List<RoomType> getAllRoomTypes() {

    List<RoomType> roomTypeList = roomTypeRepository.findAll();
    if (CollectionUtils.isEmpty(roomTypeList)) throw new RuntimeException("no room types found");

    return roomTypeList;
  }

  @Override
  public List<RoomType> getAllRoomTypesByPropertyId(long propertyId) {
    List<RoomType> roomTypes = roomTypeRepository.findAllByPropertyId(propertyId);

    if (CollectionUtils.isEmpty(roomTypes))
      throw new RuntimeException("no room types found for the property id " + propertyId);

    return roomTypes;
  }

  @Override
  public RoomType getRoomTypeByPropertyIdAndRoomTypeId(long propertyId, long roomTypeId) {
    return roomTypeRepository
        .findFirstByPropertyIdAndId(propertyId, roomTypeId)
        .orElseThrow(
            () ->
                new RuntimeException(
                    "no room types found for the combination of property id "
                        + propertyId
                        + " and roomtype id "
                        + roomTypeId));
  }

  @Override
  public RoomType createRoomType(long propertyId, RoomType roomType) {
    Property property = propertyService.getPropertyById(propertyId);
    roomType.setProperty(property);

    return roomTypeRepository.save(roomType);
  }
}
