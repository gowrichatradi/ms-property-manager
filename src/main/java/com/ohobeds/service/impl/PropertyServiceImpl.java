package com.ohobeds.service.impl;

import com.ohobeds.entity.Property;
import com.ohobeds.repository.PropertyRepository;
import com.ohobeds.service.PropertyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@RequiredArgsConstructor
@Component
public class PropertyServiceImpl implements PropertyService {

  final PropertyRepository propertyRepository;

  @Override
  public Property createProperty(Property property) {
    return propertyRepository.save(property);
  }

  @Override
  public List<Property> getProperties() {

    List<Property> allProperties = propertyRepository.findAll();
    if (CollectionUtils.isEmpty(allProperties)) {
      throw new RuntimeException("no properties found");
    }

    return allProperties;
  }

  @Override
  public Property getPropertyById(Long propertyId) {
    return propertyRepository
        .findFirstById(propertyId)
        .orElseThrow(() -> new RuntimeException("Cannot find the property with id: " + propertyId));
  }
}
