package com.ohobeds.service;

import com.ohobeds.entity.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerService {

  List<Customer> getAllCustomers();

  Customer getCustomer(long id);

  Customer getCustomerByPhoneNumber(String phoneNumber);

  Customer createCustomer(Customer customer);

  Customer updateCustomer(Customer customer);
}
