package com.ohobeds.service;

import com.ohobeds.entity.RoomType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoomTypeService {
  RoomType getRoomTypeById(long roomTypeId);

  List<RoomType> getAllRoomTypes();

  List<RoomType> getAllRoomTypesByPropertyId(long propertyId);

  RoomType getRoomTypeByPropertyIdAndRoomTypeId(long propertyId, long roomTypeId);

  RoomType createRoomType(long propertyId, RoomType roomType);
}
