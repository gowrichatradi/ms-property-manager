package com.ohobeds.service;

import com.ohobeds.entity.Property;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PropertyService {
  Property createProperty(Property property);
  List<Property> getProperties();
  Property  getPropertyById(Long propertyId);

}
