package com.ohobeds.service;

import com.ohobeds.entity.RoomTypePrice;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoomTypePriceService {

  List<RoomTypePrice> getAllRoomTypePrices();

  List<RoomTypePrice> getPricesByRoomType(long roomTypeId);

  RoomTypePrice getRoomTypePriceById(long roomTypePriceId);

  RoomTypePrice createRoomTypePrice(long roomTypeId, RoomTypePrice roomTypePrice);
}
