package com.ohobeds.service;

import com.ohobeds.entity.Customer;
import com.ohobeds.entity.Reservation;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.List;

@Service
public interface ReservationService {

  List<Reservation> findAll();

  List<Reservation> findAllByPropertyId(long propertyId);

  List<Reservation> findAllByCustomer(Customer customer);

  Reservation addReservation(
      long propertyId, long roomTypeId, long roomId, @Valid Reservation reservation);
}
