package com.ohobeds.service;

import com.ohobeds.entity.Facility;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FacilityService {

    List<Facility> getAllFacilities();

  Facility getFacilityById(long facilityId);

  Facility createFacility(Facility facility);
}

