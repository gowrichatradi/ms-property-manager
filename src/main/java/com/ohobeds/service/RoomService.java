package com.ohobeds.service;


import com.ohobeds.entity.Room;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoomService {

  List<Room> getAllRooms();

  Room getRoomById(long roomId);

  List<Room> getAllRoomsByPropertyId(long propertyId);

  List<Room> getAllRoomsByRoomTypeId(long roomTypeId);

  List<Room> getAllRoomsByPropertyIdAndRoomTypeId(long propertyId, long roomTypeId);

  Room createRoom(long propertyId, long roomTypeId, Room room);


}
